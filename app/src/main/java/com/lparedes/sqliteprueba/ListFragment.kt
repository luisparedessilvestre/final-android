package com.lparedes.sqliteprueba

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.lparedes.sqliteprueba.databinding.FragmentListBinding
import com.lparedes.sqliteprueba.entidades.Tabla
import com.lparedes.sqliteprueba.modelo.TablaDAO
import com.lparedes.sqliteprueba.util.AdaptadorPersonalizado


class ListFragment : Fragment() {

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!

    var tablaDAO = TablaDAO(requireContext())
    var listaTabla: List<Tabla> = ArrayList()
    var adaptador: AdaptadorPersonalizado? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mostrarTabla()
        asignarReferencia()
    }

    private fun mostrarTabla() {
        tablaDAO.abrirBD()
        listaTabla = tablaDAO.cargarTabla()
        adaptador = AdaptadorPersonalizado(requireContext(), listaTabla)
        binding.rvTabla.adapter = adaptador
        binding.rvTabla.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun asignarReferencia() {
        binding.btnNuevo.setOnClickListener { v: View? ->
            val intent = Intent(requireContext(), MainActivity::class.java)
            startActivity(intent)
        }
    }

    companion object {
        fun newInstance(): ListFragment {
            val fragment = ListFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}