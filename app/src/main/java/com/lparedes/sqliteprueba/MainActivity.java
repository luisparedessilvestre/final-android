package com.lparedes.sqliteprueba;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import com.lparedes.sqliteprueba.databinding.ActivityMainBinding;
import com.lparedes.sqliteprueba.entidades.Tabla;
import com.lparedes.sqliteprueba.maps.MapsUPNActivity;
import com.lparedes.sqliteprueba.modelo.TablaDAO;

public class MainActivity extends AppCompatActivity {

    Tabla tabla;
    boolean modificar = false;
    int id;

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        asignarEventos();
        verificarModifica();
    }

    private void verificarModifica(){
        if (getIntent().hasExtra("pid")){
            modificar = true;
            id = Integer.parseInt(getIntent().getStringExtra("pid"));
            binding.txtCorreo.setText(getIntent().getStringExtra("pcorreo"));
            binding.txtUsuario.setText(getIntent().getStringExtra("pusuario"));
            binding.txtTicket.setText(getIntent().getStringExtra("pticket"));
            binding.txtCelular.setText(getIntent().getStringExtra("pcelular"));
        }
    }

    private void asignarEventos(){
        binding.btnRegistrar.setOnClickListener(v -> {
            if(capturarDatos()){
                TablaDAO tablaDAO = new TablaDAO(this);
                tablaDAO.abrirBD();
                String mensaje;
                if (!modificar){
                    mensaje = tablaDAO.registrarTabla(tabla);
                }else {
                    mensaje = tablaDAO.modificarTabla(tabla);
                }
                mostrarMensaje(mensaje);
            }
        } );

        binding.btnUPN.setOnClickListener( listener -> {
            Intent i = new Intent(this, MapsUPNActivity.class);
            startActivity(i);
        });
    }
    private void  mostrarMensaje(String mensaje){
        AlertDialog.Builder ventana = new AlertDialog.Builder(this);
        ventana.setTitle("Mensaje informativo");
        ventana.setMessage(mensaje);
        ventana.setPositiveButton("Aceptar", (dialog, which) -> {
            Intent intent = new Intent(this, ListarActivity.class);
            startActivity(intent);
        });
        ventana.create().show();
    }
    private boolean capturarDatos(){
        String correo = binding.txtCorreo.getText().toString();
        String usuario = binding.txtUsuario.getText().toString();
        String ticket = binding.txtTicket.getText().toString();
        String celular = binding.txtCelular.getText().toString();
        boolean valida = true;
        if (correo.equals("")){
            binding.txtCorreo.setError("Correo es requerido");
            valida = false;
        }
        if (usuario.equals("")){
            binding.txtUsuario.setError("Usuario es requerido");
            valida = false;
        }
        if (ticket.equals("")){
            binding.txtTicket.setError("Ticket es requerido");
            valida = false;
        }
        if (celular.equals("")){
            binding.txtCelular.setError("Celular es requerido");
            valida = false;
        }
        if (valida){
            if(modificar == false){
                tabla = new Tabla(correo,usuario,ticket,Integer.parseInt(celular));
            }else{
                tabla = new Tabla(id,correo,usuario,ticket,Integer.parseInt(celular));
            }
        }
        return valida;
    }
}