package com.lparedes.sqliteprueba.entidades;

public class Tabla {

    private int id;
    private String correo, usuario, ticket;
    private int celular;

    public Tabla(String correo, String usuario, String ticket, int celular) {
        this.correo = correo;
        this.usuario = usuario;
        this.ticket = ticket;
        this.celular = celular;
    }

    public Tabla(int id, String correo, String usuario, String ticket, int celular) {
        this.id = id;
        this.correo = correo;
        this.usuario = usuario;
        this.ticket = ticket;
        this.celular = celular;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }
}
