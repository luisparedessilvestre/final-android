package com.lparedes.sqliteprueba.modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Debug;
import android.util.Log;

import com.lparedes.sqliteprueba.entidades.Tabla;
import com.lparedes.sqliteprueba.util.TablaBD;

import java.util.ArrayList;
import java.util.List;

public class TablaDAO {
    TablaBD tablaBD;
    SQLiteDatabase db;
    Context context;

    public TablaDAO(Context context){
        tablaBD = new TablaBD(context);
        this.context = context;
    }

    public void abrirBD(){
        db = tablaBD.getWritableDatabase();
    }
    public String registrarTabla(Tabla tabla){
        String rpta = "";
        try{
            ContentValues values = new ContentValues();
            values.put("correo", tabla.getCorreo());
            values.put("usuario", tabla.getUsuario());
            values.put("ticket", tabla.getTicket());
            values.put("celular", tabla.getCelular());
            long result = db.insert("tabla1", null, values);
            if (result ==-1){
                rpta = "Ocurrio un error al insertar";
            }else {
                rpta = "Se registro correctamente";
            }

        }catch (Exception e){
            Log.d("==>", e.toString());
        }
        return rpta;
    }
    public String modificarTabla(Tabla tabla){
        String rpta = "";
        try{
            ContentValues values = new ContentValues();
            values.put("correo", tabla.getCorreo());
            values.put("usuario", tabla.getUsuario());
            values.put("ticket", tabla.getTicket());
            values.put("celular", tabla.getCelular());
            long result = db.update("tabla1", values,"id="+tabla.getId(),null);
            if (result ==-1){
                rpta = "Ocurrio un error al actualizar";
            }else {
                rpta = "Se actualizó correctamente";
            }

        }catch (Exception e){
            Log.d("==>", e.toString());
        }
        return rpta;
    }

    public String eliminarArchivo(int id){
        String rpta = "";
        try{
            long result = db.delete("tabla1","id="+id,null);
            if (result ==-1){
                rpta = "Ocurrio un error al eliminar";
            }else {
                rpta = "Se eliminó correctamente";
            }
        }catch (Exception e){
            Log.d("==>", e.toString());
        }
        return rpta;
    }

    public List<Tabla> cargarTabla(){
        List<Tabla> listaTabla = new ArrayList<>();
        try {
            Cursor c = db.rawQuery("SELECT * FROM tabla1",null);
            while (c.moveToNext()){
                listaTabla.add(new Tabla(c.getInt(0), c.getString(1), c.getString(2), c.getString(3), c.getInt(4)));
            }
        }catch (Exception e){
            Log.d("==>", e.toString());
        }
        return listaTabla;
    }

}
