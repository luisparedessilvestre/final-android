package com.lparedes.sqliteprueba.util;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.lparedes.sqliteprueba.ListarActivity;
import com.lparedes.sqliteprueba.MainActivity;
import com.lparedes.sqliteprueba.R;
import com.lparedes.sqliteprueba.entidades.Tabla;
import com.lparedes.sqliteprueba.modelo.TablaDAO;

import java.util.ArrayList;
import java.util.List;

public class AdaptadorPersonalizado extends RecyclerView.Adapter<AdaptadorPersonalizado.MiViewHolder> {

    private Context context;
    private List<Tabla> listaTabla = new ArrayList<>();

    public AdaptadorPersonalizado(Context context, List<Tabla> listaTabla){
        this.context = context;
        this.listaTabla = listaTabla;
    }

    @NonNull
    @Override
    public AdaptadorPersonalizado.MiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View vista = inflater.inflate(R.layout.fila,parent,false);
        return new MiViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorPersonalizado.MiViewHolder holder, int position) {

        holder.filaCorreo.setText(listaTabla.get(position).getCorreo()+"");
        holder.filaUsuario.setText(listaTabla.get(position).getUsuario()+"");
        holder.filaTicket.setText(listaTabla.get(position).getTicket()+"");
        holder.filaCelular.setText(listaTabla.get(position).getCelular()+"");
        holder.filaEditar.setOnClickListener(v -> {
            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra("pid",listaTabla.get(position).getId()+"");
            intent.putExtra("pcorreo",listaTabla.get(position).getCorreo()+"");
            intent.putExtra("pusuario",listaTabla.get(position).getUsuario()+"");
            intent.putExtra("pticket",listaTabla.get(position).getTicket()+"");
            intent.putExtra("pcelular",listaTabla.get(position).getCelular()+"");

            context.startActivity(intent);
        });
        holder.filaEliminar.setOnClickListener(v -> {
            AlertDialog.Builder ventana = new AlertDialog.Builder(context);
            ventana.setTitle("Mensaje informativo");
            ventana.setMessage("¿Desea eliminar la información?");
            ventana.setNegativeButton("NO", null);
            ventana.setPositiveButton("SI", (dialog, which) -> {
                TablaDAO tablaDAO = new TablaDAO(context);
                tablaDAO.abrirBD();
                String mensaje = tablaDAO.eliminarArchivo(listaTabla.get(position).getId());

                AlertDialog.Builder v1 = new AlertDialog.Builder(context);
                v1.setTitle("Mensaje informativo");
                v1.setMessage(mensaje);
                v1.setPositiveButton("Aceptar",(dialog1, which1) -> {
                    Intent intent = new Intent(context, ListarActivity.class);
                    context.startActivity(intent);
                });
                v1.create().show();

            });
            ventana.create().show();
        });
    }

    @Override
    public int getItemCount() {
        return listaTabla.size();
    }

    public class MiViewHolder extends RecyclerView.ViewHolder{

        TextView filaCorreo, filaUsuario, filaTicket, filaCelular;

        ImageButton filaEditar, filaEliminar;

        public MiViewHolder(@NonNull View itemView) {
            super(itemView);
            filaCorreo = itemView.findViewById(R.id.filaCorreo);
            filaUsuario = itemView.findViewById(R.id.filaUsuario);
            filaTicket = itemView.findViewById(R.id.filaTicket);
            filaCelular = itemView.findViewById(R.id.filaCelular);
            filaEditar = itemView.findViewById(R.id.filaEditar);
            filaEliminar = itemView.findViewById(R.id.filaEliminar);
        }
    }
}
