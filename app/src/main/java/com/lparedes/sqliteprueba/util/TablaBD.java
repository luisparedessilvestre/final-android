package com.lparedes.sqliteprueba.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TablaBD extends SQLiteOpenHelper {

    public  TablaBD(Context context){
        super(context,"tabla1.db",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query =
                "CREATE TABLE tabla1"+
                " (id INTEGER PRIMARY KEY AUTOINCREMENT,"+
                " correo TEXT NOT NULL,"+
                " usuario TEXT NOT NULL,"+
                " ticket TEXT NOT NULL,"+
                " celular INTEGER NOT NULL);";

        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
